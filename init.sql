CREATE TABLE table1 (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE table2 (
    id SERIAL PRIMARY KEY,
    table1_id INT NOT NULL,
    value VARCHAR(255) NOT NULL,
    FOREIGN KEY (table1_id) REFERENCES table1(id)
);

CREATE TABLE table3 (
    id SERIAL PRIMARY KEY,
    table2_id INT NOT NULL,
    description TEXT NOT NULL,
    FOREIGN KEY (table2_id) REFERENCES table2(id)
);

-- Insérer des données dans table1
INSERT INTO table1 (name) VALUES
('Item A'),
('Item B'),
('Item C'),
('Item D'),
('Item E'),
('Item F'),
('Item G'),
('Item H'),
('Item I'),
('Item J');

-- Insérer des données dans table2
INSERT INTO table2 (table1_id, value) VALUES
(1, 'Value 1'),
(2, 'Value 2'),
(3, 'Value 3'),
(4, 'Value 4'),
(5, 'Value 5'),
(6, 'Value 6'),
(7, 'Value 7'),
(8, 'Value 8'),
(9, 'Value 9'),
(10, 'Value 10');

-- Insérer des données dans table3
INSERT INTO table3 (table2_id, description) VALUES
(1, 'Description 1'),
(2, 'Description 2'),
(3, 'Description 3'),
(4, 'Description 4'),
(5, 'Description 5'),
(6, 'Description 6'),
(7, 'Description 7'),
(8, 'Description 8'),
(9, 'Description 9'),
(10, 'Description 10');

